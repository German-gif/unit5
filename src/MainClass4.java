
public class MainClass4 {

	public static void main(String[] args) {
		Rectangle r = new Rectangle();
		System.out.println(r);
		r.moveTo(4, 4);
		System.out.println(r);
		
		Rectangle r2 = new Rectangle(new Point (1,1), 8, 6);
		System.out.println(r2);
		
		Rectangle r3 = new Rectangle(new Point(2,2),
									new Point(4,5));
		System.out.println(r3);
		
		Segment s = new Segment(new Point(2,2), new Point(5,8));
		System.out.println(s);
		Rectangle r4 = new Rectangle(s);
		System.out.println(r4);
		System.out.println(r4.getArea());
		System.out.println(r4.getPoint());
		Point p = r4.getPoint();
		System.out.println(p);
		
		Rectangle r5 = new Rectangle(new Point (5,6), 8,6);
		System.out.println("Top-Left: "+r5.getTopLeftPoint()+" Top-Rigth: "+r5.getTopRigthPoint()+" Bottom-Left: "+r5.getBottomRigthPoint());
		
		
	}
	
}
