
public class Book {
	private String title;
	private Author author;
	private double price;
	private int qtyInStok;
	
	public Book (String title, Author author, double price) {
		this.title = title;
		this.author = author;
		qtyInStok = 0;
		
	}

	public Book (String title, Author author, double price,
			int qtyInStock) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.qtyInStok = qtyInStock;

	
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyInStok() {
		return qtyInStok;
	}

	public void setQtyInStok(int qtyInStok) {
		this.qtyInStok = qtyInStok;
	}

	public String getTitle() {
		return title;
	}

	public Author getAuthor() {
		return author;
	}

	@Override
	public String toString() {
		return title + " by " + author.toString();
	}
}
