import java.util.*;
public class WordGuess {
	
	public static final int MAX_SHOOTS = 10;

	public static void main(String[] args) {
		char letter;
		boolean gameOver = false;
		MagicWord magicWord = new MagicWord();
		
		Scanner scan = new Scanner(System.in);
		
		int cntr = 0;
		while (!gameOver)  {
			if (cntr==MAX_SHOOTS) {
				gameOver = true;
				System.out.println("You lose!!! ELM.exe");
			} else {
				System.out.println(magicWord);
				System.out.println("Enter a letter");
				letter = scan.next().charAt(0);
				magicWord.checkLetter(letter);
				/*if (magicWord.checkWin()) {
				System.out.println("You're a knegrejo");
				break;
				} else {
				cntr++;
				}*/
				cntr++;
			}
		}
	}

}
