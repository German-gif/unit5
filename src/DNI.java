import java.util.Scanner;

public class DNI {

	public static final char[] LETTERS = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z',
			'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' };

	private int number;
	private char letter;

	public DNI() {
		number = 0;
		letter = LETTERS[0];
		checkLetter();
	}

	public DNI(int number) {
		this.number = number;
		letter = LETTERS[number % 23];
	}

	public DNI(int number, char letter) {
		this.number = number;
		this.letter = letter;
		letter = LETTERS[number % 23];
		if (letter != LETTERS[number % 23]) {
			this.number = -number;
		}
	}

	public DNI(String s) {
		letter = s.charAt(s.length() - 1);
		number = Integer.parseInt(s.replaceAll("[^\\d]", ""));
		checkLetter();

	}

	public int getNumber() {
		return number;
	}

	public char getLetter() {
		return letter;
	}

	public void setNumber(int number) {
		this.number = number;
		checkLetter();
		
	}

	public void checkLetter() {
		if (number >= 0) {
			if (LETTERS[number % 23] != letter) {
				number = -number;
			}
		} else {
			if (LETTERS[-number % 23] == letter) {
				number = -number;
			}
		}
	}
	
	public char getletterForDni() {
		return LETTERS[number % 23];
	}
		
	public boolean isCorrect() {
		if (number >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "" + number + letter;
	}
	
	public String toFormattedString() {
		StringBuilder s = new StringBuilder(String.valueOf(number));
		if (number>999) {
			s.insert(s.length()-3, '-');
		}
		if (number>999999) {
			s.insert(s.length()-7, '-');
		}
		if (number>99999999) {
			s.insert(s.length()-11, '-');
		}
		return s.toString()+"-"+letter;
	}
	
	public static char letterForDni(int number) {
		return LETTERS[number % 23];
	}
	
	public static String NifForDni(int number) {
		
		return new DNI(number).toString();
	}
}		
		
		/*String s = "" + number;
		int length = s.length();
		
		String last3 = s.substring(s.length()-3);
		String strMiddle = s.substring(s.length()-6, s.length() - 3);
		return strMiddle + "-" + last3;*/
	

