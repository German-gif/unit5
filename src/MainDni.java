
public class MainDni {

	public static void main (String[] args) {
		DNI d1 = new DNI (12121212);
		System.out.println(d1);
		
		DNI d2 = new DNI("18.957.690-D");
		System.out.println(d2);
		System.out.println(d2.isCorrect());
		
		DNI d3 = new DNI(24552749);
		System.out.println(d3.toFormattedString());
	
		System.out.println(DNI.letterForDni(12334434));
		System.out.println(DNI.NifForDni(333));
	}
}
