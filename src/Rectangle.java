
public class Rectangle extends Point {

	private int width;
	private int heigth;
		
	public Rectangle() {
		super();
		width = 0;
		heigth = 0;
	}
	
	public Rectangle(Point p, int width, int heigth) {
		super(p.getX(), p.getY());
		this.width = width;
		this.heigth = heigth;
	}
	
	public Rectangle(Point p1, Point p2) {
		super(p1.getX(), p1.getY());
		width = p2.getX() - p1.getX();
		heigth = p2.getY() - p1.getY();
	}
	
	public Rectangle(Segment segment) {
		super(segment.getStartPoint().getX(),
				segment.getStartPoint().getY());
		Point p1 = segment.getStartPoint();
		Point p2 = segment.getEndPoint();
		width = p2.getX() - p1.getX();
		heigth = p2.getY() - p1.getY();
	}
	
	public Rectangle(int x, int y, int width, int heigth) {
		super(x,y);
		this.width = width;
		this.heigth = heigth;
	}
	
	public int getArea() {
		return width * heigth;
	}
	
	@Override
	public String toString() {
		return super.toString() + " width: " + width + " heigth: " + heigth;
	}

	public Point getPoint() {
		Point p1 = new Point(getX(),getY() );
		return p1;
	}
	
	public Point getBottomLeftPoint() {
		Point p1 = new Point(getX(),getY() );
		return p1;
	}

	public Point getTopLeftPoint() {
		Point p1 = new Point(getX(),getY()+heigth);
		return p1;
	}
	
	public Point getTopRigthPoint() {
		Point p1 = new Point(getX()+width ,getY()+heigth);
		return p1;
	}
	
	public Point getBottomRigthPoint( ) {
		Point p1 = new Point(getX()+width,getY());
		return p1;
	}

}


